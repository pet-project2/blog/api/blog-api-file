﻿using DTO.Image;
using MediatR;
using Microsoft.AspNetCore.Http;
using Repository;
using Service.Image;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Application.Image.Commands.Upload
{
    public class UploadImageCommandHandler : IRequestHandler<UploadImageCommand, UploadImageHandlerResponse>
    {
        private readonly IImageRepository _imageRepository;
        private readonly IImagePathService _imagePathService;
        public UploadImageCommandHandler(IImageRepository imageRepository, IImagePathService imagePathService)
        {
            this._imageRepository = imageRepository;
            this._imagePathService = imagePathService;
        }

        public async Task<UploadImageHandlerResponse> Handle(UploadImageCommand request, CancellationToken cancellationToken)
        {
            var formFile = request.file;
            if(formFile == null || formFile.Length <= 0)
            {
                throw new UploadImageHandlerException("Can not save empty file");
            }

            var fileName = formFile.FileName;
            var filePath = await this._imagePathService.GetFullPathWithFileAsync(fileName);
            var relativePath = await this._imagePathService.GetRelativePathWithFileAsync(fileName);
            var fileMapBaseUrl = await this._imagePathService.GetUrlFileWithApplicationUrlAsync(fileName);
            // save file

            var image = new Domain.Image
            {
                Id = Guid.NewGuid(),
                CreatedDate = DateTime.Now,
                Path = relativePath
            };

            await this.SaveImageToDb(image);
            await this.SaveImageToFolder(formFile, filePath);
            var response = new UploadImageHandlerResponse
            {
                Id = image.Id,
                Path = fileMapBaseUrl
            };

            return await Task.FromResult(response);
        }

        private async Task SaveImageToDb(Domain.Image image)
        {
            await this._imageRepository.SaveNewImage(image);
        }

        private async Task SaveImageToFolder(IFormFile formFile, string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                await formFile.CopyToAsync(stream);
            }
        }
    }
}
