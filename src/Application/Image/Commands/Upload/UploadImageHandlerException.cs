﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Image.Commands.Upload
{
    public class UploadImageHandlerException : Exception
    {
        public UploadImageHandlerException(string name): base(name)
        {

        }
    }
}
