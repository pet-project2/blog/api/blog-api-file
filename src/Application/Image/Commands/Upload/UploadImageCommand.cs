﻿using DTO.Image;
using MediatR;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Image.Commands.Upload
{
    public class UploadImageCommand : IRequest<UploadImageHandlerResponse>
    {
        public IFormFile file { get; set; }
    }
}
