﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Image
{
    public class ImagePathException : Exception
    {
        public ImagePathException(string message) : base(message)
        {

        }
    }
}
