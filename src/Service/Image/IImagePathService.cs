﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Image
{
    public interface IImagePathService : IPathService
    {
        Task<string> GetFullPathWithFileAsync(string fileName);
        Task<string> GetRelativePathWithFileAsync(string fileName);
        Task<string> GetUrlFileWithApplicationUrlAsync(string fileName);
    }
}
