﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Service.Image
{
    public class ImagePathService : IImagePathService
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _environment;
        private readonly IHttpContextAccessor _context;

        public ImagePathService(IConfiguration configuration, IWebHostEnvironment environment, IHttpContextAccessor context)
        {
            this._configuration = configuration;
            this._environment = environment;
            this._context = context;
        }

        private Task<string> GetRootPathConfigFileAsync()
        {
            var task = Task.Factory.StartNew(() =>
            {
                var path = this._configuration["File:Path:Image"];
                return path;
            });
            return task;
        }

        private async Task<string> GetUploadFolderAsync()
        {
            var configPath = await this.GetRootPathConfigFileAsync();
            var currentDateTime = DateTime.Now.ToString("yyyy-MM-dd");
            if (!string.IsNullOrEmpty(configPath))
            {
                var path = Path.Combine(configPath, currentDateTime);
                return path;
            }
            throw new ImagePathException("Can not get environmemt config path! Please check config");
        }

        private void CreateFolderIfFolderNotExist(string filePath)
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }
        public async Task<string> GetApplicationUploadPathAsync()
        {
            var configPath = await this.GetUploadFolderAsync();
            var webRootPath = this._environment.WebRootPath;
            if (!string.IsNullOrEmpty(webRootPath))
            {
                var path = Path.Combine(webRootPath, configPath);
                CreateFolderIfFolderNotExist(path);
                return path;
            }

            throw new ImagePathException("Can not get environment path! Please check config path");
        }

        public async Task<string> GetFullPathWithFileAsync(string fileName)
        {
            // absolute path on server
            var applicationPath = await this.GetApplicationUploadPathAsync();
            
            // hash file Name
            var hashFileName = this.GetFullHashFileName(fileName);

            var path = Path.Combine(applicationPath, hashFileName);
            return path;
        }

        public async Task<string> GetRelativePathWithFileAsync(string fileName)
        {
            var configPath = await this.GetUploadFolderAsync();
            var hashFileName = this.GetFullHashFileName(fileName);
            var path = Path.Combine(configPath, hashFileName);
            return path;
        }


        private string GetFullHashFileName(string file)
        {
            // get fileName(name + currentDateTime) and extension
            var fileWithoutExtension = Path.GetFileNameWithoutExtension(file) + DateTime.Now.ToLongTimeString();
            var fileExtension = Path.GetExtension(file);

            //hash file name 
            var hashFileNameBytes = SHA256.Create().ComputeHash(Encoding.UTF8.GetBytes(fileWithoutExtension.Trim()));
            var hashFileName = BitConverter.ToString(hashFileNameBytes).Replace("-", String.Empty);
            return hashFileName + fileExtension;
        }

        public async Task<string> GetUrlFileWithApplicationUrlAsync(string fileName)
        {
            var relativePath = await this.GetRelativePathWithFileAsync(fileName);
            var scheme = this._context.HttpContext.Request.Scheme;
            var contextUrl = this._context.HttpContext.Request.Host.Value;
            if (!string.IsNullOrEmpty(contextUrl) && !string.IsNullOrEmpty(scheme))
            {
                var url = Path.Combine(contextUrl, relativePath).Replace('\\', '/');
                var path = scheme + "://" + url;
                return path;
            }
            throw new ImagePathException("Can not get base url environment! Please check application config path");
        }
    }
}
