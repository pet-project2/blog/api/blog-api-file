﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO.Image
{
    public class UploadImageResponse
    {
        public int status { get; set; }
        public string message { get; set; }
        public Guid id { get; set; }
        public string path { get; set; }
    }
}
