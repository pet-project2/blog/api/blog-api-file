﻿using Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Entity
{
    public class ImageDbContext : DbContext
    {
        public ImageDbContext(DbContextOptions<ImageDbContext> options) : base(options)
        {

        }

        public DbSet<Image> Images { get; set; }
    }
}
