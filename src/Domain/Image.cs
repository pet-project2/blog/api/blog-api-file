﻿using System;

namespace Domain
{
    public class Image
    {
        public Guid Id { get; set; }
        public string Path { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
