﻿using Domain;
using System;
using System.Threading.Tasks;

namespace Repository
{
    public interface IImageRepository
    {
        Task SaveNewImage(Image image);
        Task<Image> GetImageById(Guid id);
    }
}
