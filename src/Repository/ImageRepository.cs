﻿using Domain;
using Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class ImageRepository : IImageRepository
    {
        private readonly ImageDbContext _dbContext;
        public ImageRepository(ImageDbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public async Task<Image> GetImageById(Guid id)
        {
            var image = await this._dbContext.Images.FindAsync(id);
            return image;
        }

        public async Task SaveNewImage(Image image)
        {
            this._dbContext.Images.Add(image);
            await this._dbContext.SaveChangesAsync();
        }
    }
}
