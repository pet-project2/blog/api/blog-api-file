﻿using Application.Image.Commands.Upload;
using DTO.Image;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly IMediator _mediator;
        public ImageController(IMediator mediator)
        {
            this._mediator = mediator;
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            try
            {
                var command = new UploadImageCommand
                {
                    file = file
                };
                var resuilt = await this._mediator.Send(command);
                var response = new UploadImageResponse
                {
                    id = resuilt.Id,
                    message = "Successfully",
                    path = resuilt.Path,
                    status = (int)HttpStatusCode.OK
                };
                return Ok(response);
            }
            catch (Exception ex)
            {
                var response = new UploadImageResponseError
                {
                    status = (int)HttpStatusCode.InternalServerError,
                    message = ex.Message
                };
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
    }
}
